from mongoengine import *
import datetime
import random
import utils
from Queue import PriorityQueue
from Queue import Queue
from Queue import Empty
import sys
import threading
import config
import exceptions

'''FrontEnd implemented as a MongoDB collection

There will be one FrontEnd for each node in the cluster, therefore wee need to auto generate different names
for the different FrontEnd collections because there will be all in the same distributed database.
i.e. FrontEnd_node01...FrontEnd_node99'''

# class FrontEnd(Document):
#     url = StringField()
#     date_added = DateTimeField(default=datetime.datetime.now)
#     fetched = BooleanField(default=False)
#     root_url = StringField()
#    
#     meta = {"collection": "FrontEnd",
#             "allow_inheritance":False,
#             "indexes": ["root_url"]}
#     
#     #frontier.FrontEnd().pop_first_url()
#     def pop_first_url(self, root_url1):  #need to synchronize the method
#         document=FrontEnd.objects(root_url=root_url1).first()
#         url1=document.url
#         document.delete()
#         return url1
      
      
class FrontEnd(Document):
    root_url = StringField(unique=True)
    rand = IntField(required=True)  # probably need to be unique
    urls=ListField()
   
    meta = {"collection": "F",
            "allow_inheritance":False,
            "indexes": ["rand"]}
    
    #frontier.FrontEnd().pop_first_url()
    def pop_url(self, root_url1):  #need to synchronize the method
        document = FrontEnd.objects(root_url=root_url1).first()
        FrontEnd.objects(root_url=root_url1).update_one(pop__urls=-1)
        return document.urls[0]
    
    def get_random_root(self, rand1):
        document = FrontEnd.objects(rand__gte=rand1).first()
        if (document == None):
            document = FrontEnd.objects(rand__lte=rand1).first()
        return document.root_url
    
    

#http://hg.python.org/cpython/file/2.7/Lib/Queue.py
class BackEndPriorityQueue(PriorityQueue):   
    
    def set_name(self, name):
        self.mutex.acquire()
        self.name=name
        self.mutex.release()

    def set_id(self, qid):
        self.mutex.acquire()
        self.id=qid
        self.mutex.release()
    
    def peek(self):
        self.not_empty.acquire()
        try:
            if not self._qsize():
                raise Empty
            item=self.queue[0]
            return item
        finally:
            self.not_empty.release()

class BackEndQueue(Queue):
    
    def set_name(self, name):
        self.mutex.acquire()
        self.name=name
        self.mutex.release()

    def set_id(self, qid):
        self.mutex.acquire()
        self.id=qid
        self.mutex.release()
        
    def peek(self):
        self.not_empty.acquire()
        try:
            if not self._qsize():
                raise Empty
            item=self.queue[0]
            self.not_full.notify()
            return item
        finally:
            self.not_empty.release()

class BackEnd(object):
    q_number = config.NR_OF_BACK_END_QUEUES    #.number of q Queues
    q_list=[]       #help list, we use it because there is no peek function for Queue
    Q = PriorityQueue(q_number)
    T={}            #table to map ques with hosts
    mutex = threading.Lock()
    
    multiplier = config.WAIT_TIME_MULTIPLIER
    
    @staticmethod
    def q_factory(name, qid):  #name of the queue will be tho root_url - host server
      
        q=BackEndQueue(config.BACK_END_QUEUE_SIZE)
        q.set_name(name)
        q.set_id(qid)
    
        return q
    
    
    @staticmethod  
    def init2():
        thread_name= threading.current_thread().getName()
        print thread_name+" >> ","\n\nInitialization\n\n "
        
        i=0
        j=0
        while (len(BackEnd.T)<BackEnd.q_number):
            r=random.randint(-sys.maxint - 1, sys.maxint)
            hosts=BackEnd.T.viewvalues();
            host=FrontEnd().get_random_root(r)
            while not ((host, 0) in hosts):
                i=i+1
                #print "Queue name: ", host
                q=BackEnd.q_factory(host, i)
                BackEnd.T.update({q.id:(host, 0)})
                BackEnd.q_list.append(q) ## temporary list
                
                BackEnd.fill_q(q, host)
                BackEnd.Q.put((0,q))
                hosts=BackEnd.T.viewvalues();      
        print thread_name+" >> ","\tinit ",BackEnd.q_list[0].name, " size: ",BackEnd.q_list[0].qsize(),"  ",BackEnd.q_list[1].name, " size: ",BackEnd.q_list[1].qsize(),"   ", BackEnd.q_list[2].name, " size: ",BackEnd.q_list[2].qsize(),"  "
  
        
   
    @staticmethod  
    def put_q(url_download_time, q):
     
      
        thread_name= threading.current_thread().getName()
        try:
            BackEnd.Q.put( (BackEnd.multiplier*url_download_time, q) )
            
            BackEnd.T.update({q.id:(q.name, BackEnd.multiplier*url_download_time)})
            print thread_name+" >> Put Q (",BackEnd.multiplier*url_download_time,", ", q.name,")", " Q size:", BackEnd.Q.qsize(),"   Q:", Q.queue()
        except:
            pass
      
      

    @staticmethod
    def get_queue():
       
      
        thread_name= threading.current_thread().getName()
        try:
            #print "Q size: " , BackEnd.Q.qsize()-1
            return BackEnd.Q.get(False)
        except:           
            #print thread_name+" >> ","Q empty, return None", BackEnd.Q.qsize()
            return None
   
       

    #refill queue. if it is provided a queue name (a host), fill with that host (this is how is made in init).  If not fill the queue with random host. this is the normal filling procees  
    @staticmethod
    def fill_q(q, host=None):
       
        thread_name= threading.current_thread().getName()
        while (q.empty()):
            # sometimes w
            r=random.randint(-sys.maxint - 1, sys.maxint)
            count=0
            u=None
            while not q.full():
                try:
                    if(host==None):
                        hosts=BackEnd.T.viewvalues()
                        r=random.randint(-sys.maxint - 1, sys.maxint)
                        hosts=BackEnd.T.viewvalues();
                        root_url=FrontEnd().get_random_root(r)
                        
                        
                        while not (root_url in hosts) and not q.full():
                            u=root_url
                        
                            while (root_url=="data") and (root_url=="clsid"):
                                r=random.randint(-sys.maxint - 1, sys.maxint)
                                root_url=FrontEnd().get_random_root(r)
                            url=FrontEnd().pop_url(root_url)
                            q.name=utils.get_host(url)
                            q.put(url)
                            count=count+1
                            print thread_name+" >> ","filled queue", q.name, " with ", url 
                    else:
                        u=host
                        url=FrontEnd().pop_url(host)
                        q.put(url)
                        count=count+1
                        
                        print thread_name+" >> ","filled queue", q.name, " with ", url
                except (exceptions.IndexError):
                    print thread_name+" >> ","\tException, not enough urls to fill ", u, "only ", count, " ",r,"  ",sys.exc_info()[0]
                    break
                except: 
                    break

    @staticmethod
    def fill_q2(q, host=None):
        thread_name= threading.current_thread().getName()
        while (q.empty()):

            r=random.randint(-sys.maxint - 1, sys.maxint)
            count=0
            u=None
            while not q.full():
                try:
                    if(host==None):
                        r=random.randint(-sys.maxint - 1, sys.maxint)
                        print r
                        root_url=FrontEnd().get_random_root(r)
                        while (root_url=="data"):
                            r=random.randint(-sys.maxint - 1, sys.maxint)
                            root_url=FrontEnd().get_random_root(r)
                        url=FrontEnd().pop_url(root_url)
                        q.name=utils.get_host(url)
                        q.put(url)
                        count=count+1
                        u=root_url
                        print thread_name+" >> ","filled queue", q.name, " with ", url
                    else:
                        u=host
                        url=FrontEnd().pop_url(host)
                        q.put(url)
                        count=count+1
                       
                        print thread_name+" >> ","filled queue", q.name, " with ", url
                except:
                    print thread_name+" >> ","\tException, not enough urls to fill ", u, "only ", count
                    break



    
    
    
    