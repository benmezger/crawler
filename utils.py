from re import compile, IGNORECASE
from BeautifulSoup import BeautifulSoup
import urlparse
import re

reULR= compile(
            r'^(?:http|ftp)s?://' # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
            r'localhost|' #localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?' # optional port
            r'(?:/?|[/?]\S+)$', IGNORECASE)

def check_url(url):
    '''Check if url is valid'''
    if not url:
       return
    if url.startswith("www."):
        url = "http://" + url
    matched_url = reULR.match(url)
    if matched_url:
        return matched_url.group()
    return

#def check_url(url):
    #parse_url = urlparse.urlparse(url)
    #if parse_url.hostname:
        #return parse_url.hostname
    #else:
        #print "Check_url(%s) returning argument" % url
        #return url
        
def join_url(root, child):
    root = check_url(root)
    if root.endswith("/") and not child.startswith("/"):
        return root + child
    elif not root.endswith("/") and child.startswith("/"):
        return root + child
    elif not root.endswith("/") and not child.startswith("/"):
        return root + "/" + child
    elif root.endswith("/") and child.startswith("/"):
        return root[:-1] + child

#def join_url(root, child):
    #'''Join the root url (google.com/) with the child (/images) -> http://google.com/images'''
    #root = check_url(root)
    #if root:
        #return urlparse.urljoin(root, child) 
    #return ""

def fix_bad_html(html):
    '''Perhaps there is a broken HTML we are parsing, so let's try to fix it..'''
    print "Returning beauty HTML"
    return BeautifulSoup(html).prettify()

def find_html_encoding(source):
    '''Todo function, try to find encoding in the html'''
    xpath = "//meta/@content|//meta/@content"
    path = lxml.html.fromstring(source).xpath(xpath)
    ####### todo

def check_file_difference(one, two):
    '''Check difference between current robot and saved robot in the database'''
    one, two = iter(one), iter(two)
    differences = 0
    for a, b in zip(one, two):
        if a != b:
            differences += 1
    if differences <= 0:
        return False
    else:
        return True


def get_host(url):
    p = '(?:http.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
    try:
        m = re.search(p, url)
        host = m.group('host')
        return host
    except:
        #print "Problem with getting host from", url
        pass
    
    
import time
import threading 

class Timestamp(object):
    def __init__(self, url_download_time):
        self.limit = time.time() + url_download_time
        self.timer=None
        self.run()
        
    def run(self):
        t=threading.Thread(target=self.countdown, args=())
        t.start()
       
    def countdown(self):
        while True:
            current_time = time.time()
            self.timer=self.limit-current_time
            if not current_time < self.limit:
                break
    
