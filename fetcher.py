import config
import requests
import utils
from rq import Queue
from redis import Redis
import lxml.html
import robots
import logger
import parser
import schemadesign
import frontier
from mongoengine import *
import queues

import random
import time
import threading
import sys
from datetime import datetime

log = logger.Log("crawler.log")
connect(config.DB_NAME)
schemadesign.WebPage.drop_collection()
schemadesign.WebSite.drop_collection()
schemadesign.Robot.drop_collection()
frontier.FrontEnd.drop_collection()

''' !!! Don't forget to make all writes asynchronous !'''

class Crawler(object):
    def __init__(self, encoding="utf-8"):
        self.url_session = requests.Session() # Create a requests session 
        self.url_session.headers = config.USER_AGENT # Custom user-agent
        self.encoding = encoding 

    def download(self, url):
        thread_name= threading.current_thread().getName()
        #print thread_name+"  >>  Downloading ->", url
        url = utils.check_url(url) # check valid url
        print url
        robots_checker = robots.CheckRobots(utils.get_host(url), url) # Check if url is not in robots.txt
        # we need  a better get_host function which should return all the time string
      
        if robots_checker.can_open():
            try:  #in case the url is not valid, exception
                
                start_time = time.time()
                page = self.url_session.get(url) # Add url to requests session!
                url_download_time = time.time() - start_time
                
                  
                #parse page. Extract links and save them to DB
                parser_obj = parser.Parser(url, page)
                total_urls=parser_obj.parse()
                print thread_name+" >>  Downloaded ->", url, "  ", url_download_time, " Total URLs parsed: ", total_urls
                
                #  save the page features >> Try it in Mongo:  db.WebPage.find({},{text:0}).pretty()  does not show the text field which is verry long
                web_page = schemadesign.WebPage()
                web_page.url=page.url
                web_page.text=page.text
                web_page.status_code=page.status_code
                web_page.total_urls=total_urls
                web_page.fetched=True
                web_page.dt=url_download_time
                web_page.save()
                
                #save the site features  >>  Try it in Mongo: db.WebSite.find().pretty()
                web_site = schemadesign.WebSite()
                try:
                    web_site.root_url=utils.get_host(page.url)
                    #weeb_site.robot_file=robot           #Need a robot object   
                    web_site.save()
                    web_site.update_pages(utils.get_host(page.url), web_page)
                except:
                    web_site.update_pages(utils.get_host(page.url), web_page)
            

            except:
                print thread_name+" >> Exception: downlaod"
                pass
            return url_download_time
        else:
            raise Exception()
     
        #Queue = queues.CrawlerQueue()
        
     
    
    def crawl(self):
        thread_name= threading.current_thread().getName()

        for i in range(40):  # need to decide when to stop crawling
            
    
            print ""
            print ""
            
            
            
            print thread_name+">> Round#",i,"  ",">> Q: "
            for qq in frontier.BackEnd.Q.queue:
                print thread_name+">> Round#",i,"  ",">>\t ", qq[1].name
            print thread_name+">> Round#",i,"  ",">> queues:"
            for queue in frontier.BackEnd.q_list:
                print thread_name+">> Round#",i,"  ",">> \tqueue:\t",queue.name, "size:",queue.qsize()
            
            
            print thread_name+">> Round#",i,"  ",">> T:"," \t\t",frontier.BackEnd.T, "\tQ size:",frontier.BackEnd.Q.qsize()
            
            
            q=frontier.BackEnd.get_queue()
            if(q[1].empty()):
                print thread_name+" >> ", "get queue ", q[1].name, "qsize: ", q[1].qsize()
                print thread_name+" >>",q[1].name, "  empty"
                frontier.BackEnd.fill_q(q[1])
                url=q[1].get(False)
                print thread_name+" >> ","preparing to download!!: ",url, "from queue: ", q[1].name
                try:
                    url_download_time=self.download(url)
                    frontier.BackEnd.put_q(url_download_time, q[1])         
                except:
                    print thread_name+">> Round#",i,"  "," >> Exception Download, URL skipped:", url
                    frontier.BackEnd.put_q(q[0]/10, q[1])
                
            else:
                print thread_name+" >> ", "get queue ", q[1].name, "qsize: ", q[1].qsize()
                url=q[1].get(False)
                print thread_name+" >> ","preparing to download: ",url, "from queue: ", q[1].name
                try:
                    url_download_time=self.download(url)
                    frontier.BackEnd.put_q(url_download_time, q[1], ) 
                except:
                    print thread_name+">> Round#",i,"  "," >> Exception Download, URL skipped:", url
                    frontier.BackEnd.put_q(q[0]/10, q[1])
            
            
                                
    def start(self, seed, nr_threads):   #the seed will be a list in the future
        time= datetime.time(datetime.now())
        print time
         
        print "\u03A3"
         
        self.download(seed)
        
        frontier.BackEnd.init2()

              
     
        
        for i in range(nr_threads):
            i=i+1
            t = threading.Thread(target=self.crawl, args=()) 
            name = "Thread "+str(i)
            t.setName(name)
            t.start()

if __name__ == "__main__": # Debug

    threads_nr=config.NR_OF_THREADS
    c = Crawler()
    c.start("http://google.com", threads_nr)


