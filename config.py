'''Very important file. Here we set the configurations of the crawler, later, when we have a user interface, these settings will be
modified by the user, according to his needs.'''

import datetime

USER_AGENT = {"User-Agent": "R2-D2",
              "From": "crawler@crawler.com"}

DATE = datetime.datetime.now().strftime("%H:%M:%S %d-%m-%Y")
IGNORE_URL = ["#"]
MAX_QUEUE_SIZE = 100
ACCEPTED_STATUS_CODE = (200, 202)
DB_HOST = "localhost"
DB_PORT = 27017
DB_NAME = "crawler"
DB_USERNAME = None
DB_PASSWORD = None
DB_READ_PREFERENCE = False
DB_IS_SLAVE = False
#DB_COLLECTIONS = ('crawler', 'robots')
DB_COLLECTIONS = ('WebSites', 'WebPages')

#Frontier
NR_OF_THREADS = 5
NR_OF_BACK_END_QUEUES = 15
BACK_END_QUEUE_SIZE = 5
WAIT_TIME_MULTIPLIER = 1
