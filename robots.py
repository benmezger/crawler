from datetime import datetime
from mongoengine import *
from reppy.cache import RobotsCache
from reppy.parser import Rules
import schemadesign
import config
import logger
import reppy.exceptions
import reppy.parser
import time
import urllib2
import urlparse
import utils
import threading
#import robotparser
#import reppy

log = logger.Log("robots.log")

class CheckRobots(object):
    
    def __init__(self, url='', to_crawl=''):
        ''' '''
        self._accept_status = config.ACCEPTED_STATUS_CODE
        self.url = url # Parent url
        self.to_crawl = to_crawl # Url to crawl
        self.user_agent = config.USER_AGENT['User-Agent']
        self.robot_session = CustomReppy(user_agent=self.user_agent) # Create a robot session 
        if self.url != "" or self.to_crawl != "":
            self.can_open() # Check if we can crawl the url
    
    def check_robots_diff(self): # todo
        '''We save the robots.txt to our database, if we have the robots.txt in our database already, let's see if there is any different
           if there is, let's replace it'''
        print "Checking robots_diff.."
        if self.robot_session.robot_file: # If we have a robot_file from the current session..
            for robots in schemadesign.Robot.objects(url=self.url): # Find the url robot from the database..
                if utils.check_file_difference(robots.robot_file, self.robot_session.robot_file): # Check difference..
                    schemadesign.save_robots(self.url, self._format_time(self.robot_session.parsed), self.robot_session.robot_file) # Replaced it!
                    #print "[Robots.py] - %s Robots.txt has changed, updating database." % self.url

    def can_open(self):
        '''Can we crawl the path?'''
        thread_name= threading.current_thread().getName()
        #print "Adding to cache ->", self.to_crawl
        if utils.check_url(self.to_crawl): # Valid url?
            cached_robot = self.robot_session.cache(self.to_crawl) # Add url to robot_session's cache! 
            sitemaps, ttl, status = [], -1, -1 # init sitemaps, ttl and status_code..
            if cached_robot.sitemaps: # We found a sitemaps in the robots.txt?
                sitemaps = cached_robot.sitemaps
            if cached_robot.ttl: # We have a ttl from the session?
                ttl = cached_robot.ttl
            if cached_robot.status: # We have any status_code? Perhaps robots.txt is a 404?
                status = cached_robot.status
            self.save_robots(cached_robot.url, cached_robot.robot_file, sitemaps, ttl, status)
        
            if cached_robot.allowed(self.to_crawl): # Are we allowed to crawl this path?
                #print thread_name+" >> [Robots.txt] %s allows you to crawl %s" % (self.url, self.to_crawl)
                return True
            else:
                print thread_name+" >> [Robots.txt] %s doesn't allow you to crawl %s" % (self.url, self.to_crawl)
                return False
        else:
            print thread_name+" >> [Robots.py] self.to_crawl is not a valid URL"
            return False

        return False

    def save_robots(self, url, file, sitemaps, ttl, status):
        '''Save robots.txt for database, this will be reimplemented later, so we can figure out the average that the robots.txt file changes so we can
           update accordingly'''
        if status not in self._accept_status:
            file = ""
            sitemaps = ""
            reason = "Status not 200 or 202"
        if url and ttl and status:
            reason = ""
            schemadesign.save_robots(url, sitemaps, ttl, file, status, reason)
       
            
            #print "[Robots.py] Saved (url, file, sitemaps and ttl) to database.    url: "+url
        else:
            print "[Robots.py] Nothing to save in the database"
        return

    def _format_time(self, time):
        '''Format time for a Mongo datetime object'''
        formated_time = datetime.fromtimestamp(time)
        return formated_time

class CustomReppyRules(Rules):
    '''Custom inheritance of the reppy.Rules! -> Reason is because we want to save the robots.txt file also, reppy doesn't do that, so let's add this option'''
    def __init__(self, url, status, content, expires, user_agent=""):
        Rules.__init__(self, url, status, content, expires)
        self.user_agent = user_agent
        self.robot_file = content # content is the robots.txt, so let's save it here, so we can save it later in the CheckRobots class.
    def allowed(self, url, agent=''):
        if not agent:
            agent = self.user_agent # Reppy needs the user-agent everytime, let's add a default. 
        '''We try to perform a good match, then a * match'''
        if hasattr(url, '__iter__'):
            results = [self[agent].allowed(u) for u in url]
            return [u for u, allowed in zip(url, results) if allowed]
        return self[agent].allowed(url)

class CustomReppy(RobotsCache):
    '''Another Reppy inheritance, so we can tell RobotsCache to call our CustomReppyRules instead of reppy.Rules, also add a default user-agent'''
    def __init__(self, user_agent):
        RobotsCache.__init__(self)
        self.user_agent = user_agent # config.USER_AGENT['User-Agent']

    def cache(self, url, *args, **kwargs):
        fetched = self.fetch(url, *args, **kwargs)
        self._cache[reppy.Utility.hostname(url)] = fetched
        return fetched

    def fetch(self, url, *args, **kwargs):
        try:
            robots_url = 'http://%s/robots.txt' % reppy.Utility.hostname(url)
            reppy.logger.debug('Fetching %s' % robots_url)
            req = reppy.cache.requests.get(robots_url, *args, **kwargs)
            ttl = reppy.Utility.get_ttl(req.headers, self.default_ttl)
            return CustomReppyRules(robots_url, req.status_code, req.content, reppy.time.time() + ttl, self.user_agent)
        except Exception as exc:
            raise reppy.exceptions.ServerError(exc)
    
    def allowed(self, url, agent=''):
        if not agent:
            agent = self.user_agent
        print agent
        if hasattr(url, '__iter__'):
            results = [self.allowed(u, agent) for u in url]
            return [u for u, allowed in zip(url, results) if allowed]
        return self.find(url, fetch_if_missing=True).allowed(url, reppy.Utility.short_user_agent(agent))

#class CustomReppy(reppy.reppy):
    #def __init__(self, url=None, agentString=''):
        #reppy.reppy.__init__(self, url=url, agentString=agentString)
        #self.robot_file = None
        #self.agentString = config.USER_AGENT['User-Agent']
        #self.refresh()
        
    #def refresh(self):
        #'''Can only work if we have a url specified'''
        #if self.url:
            #try:
                #req = urllib2.Request(str(self.url), headers={'User-Agent': self.agentString})
                #page = urllib2.urlopen(req)
            #except urllib2.HTTPError as e:
                #if e.code == 401 or e.code == 403:
                     #If disallowed, assume no access
                    #logger.warn('Access disallowed to site %s' % e.code)
                    #self.parse('''User-agent: *\nDisallow: /''')
                #elif e.code >= 400 and e.code < 500:
                     #From the spec, if it's a 404, then we can proceed without restriction
                    #logger.warn('Page %s not found.' % e.url)
                    #self.parse('')
                #else:
                    #raise reppy.ReppyException, reppy.ServerError('Remote server returned status %i' % e.code)
                #return
            #except Exception as e:
                #raise reppy.ReppyException(e)
            #self.parsed = time.time()
            # Try to get the header's expiration time, which we should honor
            #self.ttl = self._get_ttl(page)
            #self.oneshot = self.ttl <= 0
            # Then parse the file
            #data = page.read()
            #self.robot_file = iter(data)
            #self.parse(data)
    
    #def allowed(self, url, agent):
        #a = self.findAgent(agent)
        #if isinstance(url, basestring):
            #return a.allowed(url)
        #else:
            #return [u for u in url if a.allowed(u)]
    
    #def disallowed(self, url, agent):
        #return not self.allowed(url, agent)

#class CustomRobotFileParser(robotparser.RobotFileParser):
    #def __init__(self, url=""):
        #robotparser.RobotFileParser.__init__(self, url)
        #self.robot_file = None
    
    #def read(self):
        #opener = robotparser.URLopener()
        #f = opener.open(self.url)
        #lines = [line.strip() for line in f]
        #self.robot_file = lines
        #f.close()
        #self.errcode = opener.errcode
        #if self.errcode in (401, 403):
            #self.disallow_all = True
        #elif self.errcode >= 400:
            #self.allow_all = True
        #elif self.errcode == 200 and lines:
            #self.parse(lines)


if __name__ == "__main__": # Debug
    urls = ["/search", "/sdch", "/groups", "/catalogs/p?", "/news/directory", "/imglanding", "/patents?vid=", "/maps/api/js?"]
    for url in urls:
        a = CheckRobots("http://www.google.com", "http://www.google.com" + url)

