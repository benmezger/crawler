from redis import Redis
import rq
from multiprocessing import Queue #, NotImplementedError
import config
import frontier
from Queue import  PriorityQueue
import heapq
import utils
import random
import sys
import time
import threading

# Some Unix systems like MacOs doesn't support the sem_getvalue() function, so it raises a NotImplementedError, since Linux does support,
# it doesn't support the NotImplementedError exception, so let's try to import it, if the machins this code is running on is a Linux, it will
# import the custom NotImplementedError error in our exceptions.py file, so when it runs on a system that doesn't support sem_getvalue() it will be raised.
try:
    from multiprocessing import NotImplementedError
except ImportError: # So import the custom
    from exceptions import NotImplementedError

class CrawlerQueue(object):
    def __init__(self, name,id, size):
        self.url_queue = Queue(size)
        self.queue_empty = True
        self.queue_full = False
        self.queue_size = 0
        self.name=name
        self.id=id

    def add_url(self, url):
        '''Add url to the queue'''
        if self.queue_size <= config.MAX_QUEUE_SIZE: # If the queue_size is less than the MAX_QUEUE_SIZE, add it!
           # print "self.queue_size is less than config.MAX_QUEUE_SIZE, adding url to the Queue! ->", self.queue_size
            self.url_queue.put(url)
            self.queue_empty = False # queue is not empty anymore, change status. 
            try: # Since MacOs doesn't have the function sem_getvalue, it does not support qsize() function.. let's try it.. 
                self.queue_size = self.url_queue.qsize()
            except NotImplementedError:
               # print "Unix error, sem_getvalue() is not implemented."
                self.queue_size += 1
            return True
        else:
           # print "self.queue_size is greater than config.MAX_QUEUE_SIZE, returning False!"
            return False
        return False

    def get_url(self):
        '''Get the url from queue'''
        if self.queue_size != 0: # Make sure that queue.get() doesn't freeze if there is nothing in the queue..
            self.queue_size -= 1
            return self.url_queue.get(False)
        else:
            return None
    
    def close_queue(self):
        '''Is the queue already the max size? Close it, so we won't accept anymore urls in it.'''
        if self.queue_size == config.MAX_QUEUE_SIZE:
            self.queue_full = True
           # print "Closing Queue.."
            self.url_queue.close()
            return True
        else:
            #print "self.url_queue is not full!"
            return False 
 
class Management(object):
    q_number = 3  #.dpreview.c number of q Queues
    q_list=[]  #help list, we use it because there is no peek function for Queue
    Q = PriorityQueue(q_number)
    T={}    #table to map ques with hosts
    lock=threading.RLock()
    
    multiplier = 10
    
    @staticmethod
    def q_factory(name, id):  #name of the queue will be tho root_url - host server
        i=0
        q=CrawlerQueue(name,id, 5)
        return q
    
    
    @staticmethod  
    def init2():
        thread_name= threading.current_thread().getName()
        i=0
        while (len(Management.T)<Management.q_number):
            r=random.randint(-sys.maxint - 1, sys.maxint)
            
            hosts=Management.T.viewvalues();
      
            host=frontier.FrontEnd().get_random_root(r)
            
            while (host in hosts) and(host != "data"):   #temp solution for handling some wired urls
                r=random.randint(-sys.maxint - 1, sys.maxint)
                host=frontier.FrontEnd().get_random_root(r)
            
            i=i+1
            
            #print "Queue name: ", host
            q=Management.q_factory(host, i)
            Management.T.update({q.id:(host, 0)})
            Management.q_list.append(q) ## temporary list
            
            Management.fill_q(q, host)
            Management.Q.put((0,q))
        print thread_name+" >> ","\tinit ",Management.q_list[0].name, " size: ",Management.q_list[0].url_queue.qsize(),"  ",Management.q_list[1].name, " size: ",Management.q_list[1].url_queue.qsize(),"   ", Management.q_list[2].name, " size: ",Management.q_list[2].url_queue.qsize(),"  "
           
#     @staticmethod
#     def init():
#         Management.init_qs()
#         Management.init_Q()
#         
#     @staticmethod
#     def init_Q():
#         
#         for i in range(Management.q_number):
#             q=Management.q_list[i]
#             Management.Q.put((0,q))
#             #heapq.heappush(Management.T, q)
#                             
#     @staticmethod
#     def init_qs():
#         T=[]
#         T=set(T)
#         
#         while (len(T)<Management.q_number):
#             r=random.randint(-sys.maxint - 1, sys.maxint)
#             a=frontier.FrontEnd().get_random_root(r)
#             T.update({a})
#         
#         
#         for i in range(Management.q_number):
#             name=T.pop()
#             print "Queue name: ", name
#             q=Management.q_factory(name)
#             Management.fill_q(q)
#             Management.q_list.append(q) 
#     
      
    @staticmethod
    def put_q(url_download_time, q):
        thread_name= threading.current_thread().getName()
        try:
            Management.Q.put( (Management.multiplier*url_download_time, q) )
            
            Management.T.update({q.id:(q.name, Management.multiplier*url_download_time)})
            print thread_name+" >> Put Q (",Management.multiplier*url_download_time,", ", q.name,")", " Q size:", Management.Q.qsize(),"   Q:", Management.Q.queue
        except:
            pass
        
       
    
    @staticmethod
    def get_queue():
        thread_name= threading.current_thread().getName()
        try:
            return Management.Q.get(False)
        except:           
            print thread_name+" >> ","Q empty, return None", Management.Q.qsize()
            return None
    
    
    #refill queue. if it is provided a queue name (a host), fill with that host (this is how is made in init).  If not fill the queue with random host. this is the normal filling procees  
    @staticmethod
    def fill_q(q, host=None):
        thread_name= threading.current_thread().getName()
        while (q.url_queue.empty()):#     @staticmethod
#     def init():
#         Management.init_qs()
#         Management.init_Q()
#         
#     @staticmethod
#     def init_Q():
#         
#         for i in range(Management.q_number):
#             q=Management.q_list[i]
#             Management.Q.put((0,q))
#             #heapq.heappush(Management.T, q)
#                             
#     @staticmethod
#     def init_qs():
#         T=[]
#         T=set(T)
#         
#         while (len(T)<Management.q_number):
#             r=random.randint(-sys.maxint - 1, sys.maxint)
#             a=frontier.FrontEnd().get_random_root(r)
#             T.update({a})
#         
#         
#         for i in range(Management.q_number):
#             name=T.pop()
#             print "Queue name: ", name
#             q=Management.q_factory(name)
#             Management.fill_q(q)
#             Management.q_list.append(q) 
#     
            # sometimes w
            r=random.randint(-sys.maxint - 1, sys.maxint)
            count=0
            u=None
            while not q.url_queue.full():
                try:
                    if(host==None):
                        root_url=frontier.FrontEnd().get_random_root(r)
                        while (root_url=="data"):
                            r=random.randint(-sys.maxint - 1, sys.maxint)
                            root_url=frontier.FrontEnd().get_random_root(r)
                        url=frontier.FrontEnd().pop_url(root_url)
                        q.name=utils.get_host(url)
                        q.add_url(url)
                        count=count+1
                        u=root_url
                        print thread_name+" >> ","filled queue", q.name, " with ", url
                    else:
                        url=frontier.FrontEnd().pop_url(host)
                        q.add_url(url)
                        count=count+1
                        u=host
                        print thread_name+" >> ","filled queue", q.name, " with ", url
                except:
                    print thread_name+" >> ","\tException, not enough urls to fill ", u, "only ", count
                    break
                
         
         