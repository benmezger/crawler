'''
Created on Apr 5, 2013

@author: sergiu
'''

import lxml.html
import utils
import config
import robots
import logger
import queues
import frontier
import random
import sys

class Parser(object):
    
    def __init__(self, url, page):
        self.url = url
        self.page = page # A requests object
        self.status_code = self.check_statusCode() # The status code from requests

    def check_statusCode(self):
        accepted_codes = (202, 200)
        if self.page.status_code in accepted_codes:
            return True
        else:
            print "[Fetcher] Status code not in accepted_codes ->", self.page.status_code
            return False
    
    def parse(self):
        urls = set(lxml.html.fromstring(utils.fix_bad_html(self.page.text)).iterlinks())
        total_urls=0
        
        
        #save allt the links, one by one in Mongo, also, fix them before saving.
        for link in urls:
            link=link[2]
            if not link.startswith("http://")  and not link.startswith("https://") or link.startswith("/"): # If url seems broken or is just a child url (/home), let's try to fix it or join it. 
                #print "Found a link that doesn't start with http or it starts with /. Trying to fix it. -> %s" % link
                link = utils.join_url(utils.get_host(self.url), link)
                #print "New url after trying to fix ->", link
            try:
                    total_urls=total_urls+1
                    doc=frontier.FrontEnd(root_url=utils.get_host(link))
                    doc.rand=random.randint(-sys.maxint - 1, sys.maxint)
                    #weeb_site.robot_file=robot           #Need a robot object   
                    doc.save()
                    frontier.FrontEnd.objects(root_url=utils.get_host(link)).update_one(add_to_set__urls=link)
            except:
                    frontier.FrontEnd.objects(root_url=utils.get_host(link)).update_one(add_to_set__urls=link)
            
     
        return total_urls   #maybe return a dict with more data inside if we need
