'''Log what the crawler is doing...'''
import logging
import os.path

class Log(object):
    def __init__(self, filename, loggername="crawler", path="logs/", formater="%(asctime)s %(levelname)s %(message)s"):
        self.logger = logging.getLogger(os.path.splitext(loggername)[0])
        self.hdlr = logging.FileHandler(path + os.path.splitext(filename)[0] + ".log")
        self.formater = logging.Formatter(formater)
        self.hdlr.setFormatter(self.formater)
        self.hdlr.addFilter(logging.Filter(name=os.path.splitext(loggername)[0]))
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.INFO)
    def info(self, msg):
        self.logger.info(msg)
    def warning(self, msg):
        self.logger.warning(msg)
    def error(self, msg):
        self.logger.error(msg)
    def critical(self, msg):
        self.logger.critical(msg)

if __name__ == "__main__":
    log = Log("test.log")
    log.info("Something is cool!")
    log.warning("Something happened!")
    log.error("Could not crawl")
    log.critical("Broken!")
