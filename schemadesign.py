from mongoengine import *
import datetime


class Robot(Document):
    url = StringField(unique=True)
    robot_file = StringField(required=True)
    sitemaps = ListField(StringField())
    ttl = FloatField()
    status = IntField()
    last_checked = DateTimeField(default=datetime.datetime.now)
    fail_reason = StringField(default="")
    meta = {"collection": "Robot",
            "allow_inheritance":False,
            "indexes": ["url", "last_checked", "robot_file"]}
  
def save_robots(url, sitemaps, ttl, file, status, reason):
    '''Save robots.txt to database and last checked date'''
    robot=Robot()
    if reason != "":
        robot.reason = reason
    robot.url = url
    robot.sitemaps = sitemaps or [""]
    robot.robot_file = "".join(file)
    robot.ttl = ttl
    robot.status = status
    try:
        robot.save()
    except:
        pass
class WebPage(Document):
    url = StringField(required=True)
    text = StringField()
    CODES =  (200, 202)
    status_code = IntField(choices=CODES) 
    total_urls = IntField()
    fetched = BooleanField()
    date_crawled  = DateTimeField(default=datetime.datetime.now)
    dt= FloatField()
   
   # root_url = StringField(max_length=200)
    meta={
    'collection':'WebPage',
    'allow_inheritance':False,
    'indexes': ['url']
    }
   
    
    #GET A PAGE BY URL		
    @queryset_manager
    def get_page_by_url(doc_cls, queryset, url1, explain=False):
        query = queryset.filter(url=url1)
        page = query.all().next()
        page_dict={'_id':page.id,'url':page.url, 'status_code':page.status_code, 'total_urls':page.total_urls, 'fetched':page.fetched}
        if explain == True :
	    print query.explain('true')
        return page_dict
        
        
    #GET THE CODE FROM A PAGE	
    @queryset_manager
    def get_code_from_page(doc_cls, queryset, url1):
        query = queryset.filter(url=url1).only('status_code')
        page = query.all().next()
        return page.status_code
        
    #GET THE NR OF URLS FROM PAGE	
    @queryset_manager
    def get_total_urls(doc_cls, queryset, url1):
        query = queryset.filter(url=url1).only('total_urls')
        page = query.all().next()
        return page.total_urls
        
    #GET FETCHED STATUS OF A PAGE
    @queryset_manager
    def get_fetch_status(doc_cls, queryset, url1):
        query = queryset.filter(url=url1).only('fetched')
        page = query.all().next()
        return page.fetched



class WebSite(Document):
    pages = ListField(ReferenceField(WebPage))
    root_url=StringField(max_length=200, unique=True)
    
    robot_file=ReferenceField(Robot)
    meta={
    'collection':'WebSite',
    'allow_inheritance':False,
    'indexes': ['root_url']
    #'indexes': ['root_url']
    }
    
    #need to handle exception, when root_url!=url1
    @queryset_manager
    def get_web_site_by_root_url(doc_cls, queryset, url1):
        query = queryset.filter(root_url=url1)
	site = query.all().next()
	pages1=[]
	for page in site.pages:
	    pages1.append(page.url)
        site_dict={'_id':site.id,'root_url':site.root_url, 'pages':pages1}
	return site_dict
	
    @queryset_manager
    def get_all_pages_from_site(doc_cls, queryset, url1):
        query = queryset.filter(root_url=url1)
	site = query.all().next()
	pages1=[]
	for page in site.pages:
	    pages1.append(page.url)
        return pages1
        
    def update_pages(website, url, page):
        WebSite.objects(root_url=url).update_one(push__pages=page)
       
        

            
     
